# Post 66 Demo

> Demo code for https://scvalex.net/posts/66/

This repo contains an example program that watches a directory of
templates for changes, runs them through
[Liquid](https://shopify.github.io/liquid/) templates, and compiles
the output with `xelatex`.

Interesting files:

- Input: [`guest_list.csv`](/guest_list.csv)
- Templates: [`invites.tex`](/templates/invites.tex) and [`invite.liquid`](/templates/invite.liquid)
- Output: [`invites.pdf`](/out/invites.pdf)
