use clap::Parser;
use liquid::model::Value;
use log::{error, info};
use serde::{Deserialize, Serialize};
use std::{
    collections::HashMap,
    fs::File,
    path::{Path, PathBuf},
    sync::mpsc,
};

type OrError<T> = Result<T, anyhow::Error>;
const TEMPLATES_DIR: &str = "templates";
const OUT_DIR: &str = "out";

#[derive(Parser)]
struct Opts {
    #[clap(long)]
    watch: bool,

    #[clap(long)]
    guest_list: String,
}

#[derive(Deserialize, Serialize)]
struct Guest {
    name: String,
    address: String,
}

fn main() -> OrError<()> {
    setup_log()?;
    let Opts { watch, guest_list } = Opts::parse();
    let mut data: HashMap<String, Value> = HashMap::new();
    let guests: Vec<Value> = csv::Reader::from_reader(File::open(guest_list)?)
        .deserialize()
        .map(|r: Result<Guest, _>| Value::Object(liquid::to_object(&r.unwrap()).unwrap()))
        .collect();
    data.insert("guests".to_string(), Value::array(guests));
    render_templates_and_compile_latex(scan_dir(TEMPLATES_DIR)?, &data)?;
    if watch {
        let updates = run_watcher(TEMPLATES_DIR)?;
        loop {
            render_templates_and_compile_latex(updates.recv()?, &data)?;
        }
    }
    Ok(())
}

fn render_templates_and_compile_latex(
    template_files: Vec<PathBuf>,
    data: &HashMap<String, Value>,
) -> OrError<()> {
    match render_templates(TEMPLATES_DIR, template_files, OUT_DIR, data) {
        Ok(()) => {
            for _ in 0..2 {
                if let Err(err) =
                    compile_latex(PathBuf::new().join(OUT_DIR).join("invites.tex"), OUT_DIR)
                {
                    error!("Failed to compile latex: {err}")
                }
            }
        }
        Err(err) => {
            error!("Failed to render templates: {err}")
        }
    }
    Ok(())
}

// Render the given templates.  Files that end with ".liquid" are partial
// templates that can be `include`d in other templates.  All other
// files are run through the template engine, and rendered to
// `out_dir`.
fn render_templates(
    in_dir: &str,
    files: Vec<PathBuf>,
    out_dir: &str,
    data: &HashMap<String, liquid::model::Value>,
) -> OrError<()> {
    use liquid::{
        partials::{EagerCompiler, InMemorySource},
        ParserBuilder, ValueView,
    };
    use rayon::prelude::*;
    info!("Rendering templates: {files:?}");
    let mut templates = InMemorySource::new();
    for f in &files {
        let path = f.strip_prefix(in_dir)?.to_str().unwrap();
        if path.ends_with(".liquid") {
            templates.add(path.to_string(), std::fs::read_to_string(f)?);
        }
    }
    let parser = ParserBuilder::new()
        .stdlib()
        .partials(EagerCompiler::new(templates))
        .build()?;
    files
        .par_iter()
        .map(|f| {
            let mut globals: HashMap<String, &dyn ValueView> = HashMap::new();
            globals.insert("data".to_string(), data as &dyn ValueView);
            let path = f.strip_prefix(in_dir)?.to_str().unwrap();
            if !path.ends_with(".liquid") {
                let out_file = PathBuf::new().join(out_dir).join(path);
                let template = parser.parse(&std::fs::read_to_string(f)?)?;
                info!("Rendering to {}", out_file.display());
                template.render_to(&mut File::create(out_file)?, &globals)?;
            }
            Ok(())
        })
        .collect::<Vec<OrError<()>>>()
        .into_iter()
        .collect::<OrError<Vec<()>>>()?;
    info!("Done rendering templates");
    Ok(())
}

// Find all files in `dir` respecting .gitignore and the like.
fn scan_dir(dir: &str) -> OrError<Vec<PathBuf>> {
    info!("Scanning {dir}");
    use ignore::Walk;
    let mut res = vec![];
    for file in Walk::new(dir) {
        let file = file?;
        if !file.metadata()?.is_file() {
            continue;
        }
        res.push(file.into_path());
    }
    Ok(res)
}

// Watch a directory for changes. If the list of files or their
// contents has changed, send the entire list of files through the
// channel.
fn run_watcher(dir: &str) -> OrError<mpsc::Receiver<Vec<PathBuf>>> {
    use notify::RecursiveMode;
    use std::time::Duration;
    let (notify_tx, notify_rx) = mpsc::channel();
    let (watcher_tx, watcher_rx) = mpsc::channel();
    let watcher_loop = {
        let dir = dir.to_string();
        move || -> OrError<()> {
            let mut debouncer =
                notify_debouncer_mini::new_debouncer(Duration::from_millis(250), None, notify_tx)?;
            let mut hashes = hash_files(scan_dir(&dir)?)?;
            debouncer
                .watcher()
                .watch(Path::new(&dir), RecursiveMode::Recursive)?;
            loop {
                match notify_rx.recv()? {
                    Err(errs) => error!("Notify errors: {errs:?}"),
                    Ok(events) if events.is_empty() => {}
                    Ok(_) => {
                        let paths = scan_dir(&dir)?;
                        let new_hashes = hash_files(paths.clone())?;
                        if hashes != new_hashes {
                            hashes = new_hashes;
                            watcher_tx.send(paths)?;
                        }
                    }
                }
            }
        }
    };
    std::thread::spawn(move || match watcher_loop() {
        Ok(()) => error!("Watcher loop ended without error"),
        Err(err) => error!("Watcher loop ended with error: {err}"),
    });
    Ok(watcher_rx)
}

fn hash_files(files: Vec<PathBuf>) -> OrError<HashMap<PathBuf, u64>> {
    let mut hashes = HashMap::new();
    for f in files {
        let hash = hash_file(&f)?;
        hashes.insert(f, hash);
    }
    Ok(hashes)
}

// Hash the contents of a path quickly.
fn hash_file<P: AsRef<Path>>(path: P) -> OrError<u64> {
    use seahash::SeaHasher;
    use std::{hash::Hasher, io::Read};
    let mut file = File::open(path.as_ref())?;
    let mut buf = [0; 4096];
    let mut hasher = SeaHasher::new();
    loop {
        match file.read(&mut buf)? {
            0 => return Ok(hasher.finish()),
            _ => hasher.write(&buf),
        }
    }
}

// Compile `path` as latex, and put the PDF and other outputs in
// `out_dir`.
fn compile_latex<P: AsRef<Path>>(path: P, out_dir: &str) -> OrError<()> {
    use std::process::Command;
    info!("Compiling {}", path.as_ref().display());
    let output = Command::new("/usr/bin/env")
        .args([
            "xelatex",
            &format!("-output-directory={out_dir}"),
            path.as_ref().to_str().unwrap(),
        ])
        .output()?;
    for line in String::from_utf8_lossy(&output.stdout)
        .split('\n')
        .filter(|s| !s.is_empty())
    {
        info!("xelatex: {line}");
    }
    for line in String::from_utf8_lossy(&output.stderr)
        .split('\n')
        .filter(|s| !s.is_empty())
    {
        error!("xelatex: {line}");
    }
    info!("Done compiling");
    Ok(())
}

fn setup_log() -> OrError<()> {
    use log::LevelFilter;
    use simplelog::{ColorChoice, Config, TermLogger, TerminalMode};
    TermLogger::init(
        LevelFilter::Info,
        Config::default(),
        TerminalMode::Stderr,
        ColorChoice::Auto,
    )?;
    Ok(())
}
